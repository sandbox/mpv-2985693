<?php

/**
 * Implements drush_hook_provision_post_COMMAND().
 */
function drush_provision_extra_sync_post_provision_verify() {
  $platform_path = pathinfo(d()->root);
  if (d()->type === 'platform' && $platform_path['basename'] == 'docroot') {
    if (provision_file()->exists($platform_path['dirname'] . '/vendor')->status()) {
      d()->service('http')->sync($platform_path['dirname'] . '/vendor');
    }
  }
}
