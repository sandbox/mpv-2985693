# Hosting Extra Sync

This is a very simple extension to provision that syncs the `vendor` directory of 
a platform to the webserver if the platform root ends with `docroot`.

Right now it's only useful for this directory structure, but in the future this
should be configurable and more flexible.
