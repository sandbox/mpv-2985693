<?php

/**
 * Implements drush_hook_provision_post_COMMAND().
 */
function drush_provision_extra_sync_post_provision_delete($backup_file = NULL) {
  $platform_path = pathinfo(d()->root);
  if (d()->type === 'platform' && $platform_path['basename'] == 'docroot') {
    if (provision_file()->exists($platform_path['dirname'] . '/vendor')->status()) {
      if (_provision_recursive_delete($platform_path['dirname'])) {
        d()->service('http')->sync($platform_path['dirname']);
        drush_log(dt("The platform directory has been removed"), 'ok');
      }
      else {
        drush_log(dt("The platform directory could not be removed"), 'error');
      }
    }
  }
}
